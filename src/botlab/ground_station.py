import sys, os, time
import lcm
import math

import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
 
import matplotlib.backends.backend_agg as agg

import pygame
from pygame.locals import *

from lcmtypes import maebot_diff_drive_t
from lcmtypes import velocity_cmd_t
from lcmtypes import pid_init_t

# Callling recently included files
from laser import *
from slam import *
from maps import *

# SLAM preferences
# CHANGE TO OPTIMIZE
USE_ODOMETRY = True
MAP_QUALITY = 3

# Laser constants
# CHANGE TO OPTIMIZE IF NECSSARY
DIST_MIN = 100; # minimum distance
DIST_MAX = 6000; # maximum distance

# Map constants
# CHANGE TO OPTIMIZE
MAP_SIZE_M = 6.0 # size of region to be mapped [m]
INSET_SIZE_M = 1.0 # size of relative map
MAP_RES_PIX_PER_M = 100 # number of pixels of data per meter [pix/m]
MAP_SIZE_PIXELS = int(MAP_SIZE_M*MAP_RES_PIX_PER_M) # number of pixels across the entire map
MAP_DEPTH = 3 # depth of data points on map (levels of certainty)

# CONSTANTS
DEG2RAD = math.pi / 180
RAD2DEG = 180 / math.pi

# KWARGS
# PASS TO MAP AND SLAM FUNCTIONS AS PARAMETER
KWARGS, gvars = {}, globals()
for var in ['MAP_SIZE_M','INSET_SIZE_M','MAP_RES_PIX_PER_M','MAP_DEPTH','USE_ODOMETRY','MAP_QUALITY']:
  KWARGS[var] = gvars[var] # constants required in modules

class MainClass:
    def __init__(self, width=640, height=480, FPS=10):
        pygame.init()
        self.width = width
        self.height = height
        self.screen = pygame.display.set_mode((self.width, self.height))
        
        # LCM Subscribe
        self.lc = lcm.LCM()
        # NEEDS TO SUBSCRIBE TO OTHER LCM CHANNELS LATER!!!

        # Prepare Figure for Lidar
        self.fig = plt.figure(figsize=[3, 3], # Inches
                              dpi=100)    # 100 dots per inch, 
        self.fig.patch.set_facecolor('white')
        self.fig.add_axes([0,0,1,1],projection='polar')
        self.ax = self.fig.gca()

        # Prepare Figures for control
        path = os.path.realpath(__file__)
        path = path[:-17] + "maebotGUI/"

        self.arrowup = pygame.image.load(path + 'fwd_button.png')
        self.arrowdown = pygame.image.load(path + 'rev_button.png')
        self.arrowleft = pygame.image.load(path + 'left_button.png')
        self.arrowright = pygame.image.load(path + 'right_button.png')
        self.resetbut = pygame.image.load(path + 'reset_button.png')
        self.arrows = [0,0,0,0]

        # PID Initialization - Change for your Gains!
        command = pid_init_t()
        command.kp = 0.0        
        command.ki = 0.0
        command.kd = 0.0
        command.iSat = 0.0 # Saturation of Integral Term. 
                           # If Zero shoudl reset the Integral Term
        self.lc.publish("GS_PID_INIT",command.encode())

        # Declare Laser, datamatrix and slam
        self.laser = RPLidar(DIST_MIN, DIST_MAX) # lidar
        self.datamatrix = DataMatrix(**KWARGS) # handle map data
        self.slam = Slam(self.laser, **KWARGS) # do slam processing
 
#  def OdoVelocityHandler(self,channel,data): 
# IMPLEMENT ME !!!
 
#  def LidarHandler(self,channel,data): 
# IMPLEMENT ME !!!


    def MainLoop(self):
        pygame.key.set_repeat(1, 20)
        vScale = 0.5

        # Prepare Text to Be output on Screen
        font = pygame.font.SysFont("DejaVuSans Mono",14)

        while 1:
            leftVel = 0
            rightVel = 0
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    sys.exit()
                elif event.type == KEYDOWN:
                    if ((event.key == K_ESCAPE)
                    or (event.key == K_q)):
                        sys.exit()
                    key = pygame.key.get_pressed()
                    if key[K_RIGHT]:
                        leftVel = leftVel + 0.40
                        rightVel = rightVel - 0.40
                    if key[K_LEFT]:
                        leftVel = leftVel - 0.40
                        rightVel = rightVel + 0.40
                    if key[K_UP]:
                        leftVel = leftVel + 0.65
                        rightVel = rightVel + 0.65
                    if key[K_DOWN]:
                        leftVel = leftVel - 0.65
                        rightVel = rightVel - 0.65
                    else:
                        leftVel = 0.0
                        rightVel = 0.0                      
                    cmd = maebot_diff_drive_t()
                    cmd.motor_right_speed = vScale * rightVel
                    cmd.motor_left_speed = vScale * leftVel
                    self.lc.publish("MAEBOT_DIFF_DRIVE",cmd.encode())
                elif event.type == pygame.MOUSEBUTTONDOWN:
                    command = velocity_cmd_t()
                    command.Distance = 987654321.0
                    if event.button == 1:
                        if ((event.pos[0] > 438) and (event.pos[0] < 510) and
                            (event.pos[1] > 325) and (event.pos[1] < 397)):
                            command.FwdSpeed = 100.0
                            command.Distance = 1000.0
                            command.AngSpeed = 0.0
                            command.Angle = 0.0
                            print "Commanded PID Forward One Meter!"
                        elif ((event.pos[0] > 438) and (event.pos[0] < 510) and
                            (event.pos[1] > 400) and (event.pos[1] < 472)):
                            command.FwdSpeed = 100.0
                            command.Distance = -1000.0
                            command.AngSpeed = 0.0
                            command.Angle = 0.0
                            print "Commanded PID Backward One Meter!"
                        elif ((event.pos[0] > 363) and (event.pos[0] < 435) and
                            (event.pos[1] > 400) and (event.pos[1] < 472)):
                            command.FwdSpeed = 0.0
                            command.Distance = 0.0
                            command.AngSpeed = 35.0
                            command.Angle = 90.0
                            print "Commanded PID Left One Meter!"
                        elif ((event.pos[0] > 513) and (event.pos[0] < 585) and
                            (event.pos[1] > 400) and (event.pos[1] < 472)):
                            command.FwdSpeed = 0.0
                            command.Distance = 0.0
                            command.AngSpeed = -35.0
                            command.Angle = -90.0
                            print "Commanded PID Right One Meter!"
                        elif ((event.pos[0] > 513) and (event.pos[0] < 585) and
                            (event.pos[1] > 325) and (event.pos[1] < 397)):
                            pid_cmd = pid_init_t()
                            pid_cmd.kp = 0.0        # CHANGE FOR YOUR GAINS!
                            pid_cmd.ki = 0.0        # See initialization
                            pid_cmd.kd = 0.0
                            pid_cmd.iSat = 0.0
                            self.lc.publish("GS_PID_INIT",pid_cmd.encode())
                            print "Commanded PID Reset!"
                        if (command.Distance != 987654321.0):
                            self.lc.publish("GS_VELOCITY_CMD",command.encode())

            self.screen.fill((255,255,255))

            # Plot Lidar Scans
            # IMPLEMENT ME - CURRENTLY ONLY PLOTS ONE DOT
            plt.cla()
            self.ax.plot(0,0,'or',markersize=2)
            self.ax.set_rmax(1.5)
            self.ax.set_theta_direction(-1)
            self.ax.set_theta_zero_location("N")
            self.ax.set_thetagrids([0,45,90,135,180,225,270,315],
                                    labels=['','','','','','','',''], 
                                    frac=None,fmt=None)
            self.ax.set_rgrids([0.5,1.0,1.5],labels=['0.5','1.0',''],
                                    angle=None,fmt=None)

            canvas = agg.FigureCanvasAgg(self.fig)
            canvas.draw()
            renderer = canvas.get_renderer()
            raw_data = renderer.tostring_rgb()
            size = canvas.get_width_height()
            surf = pygame.image.fromstring(raw_data, size, "RGB")
            self.screen.blit(surf, (320,0))

            # Position and Velocity Feedback Text on Screen
            # self.lc.handle()          
            pygame.draw.rect(self.screen,(0,0,0),(5,350,300,120),2)
            text = font.render("    POSITION    ",True,(0,0,0))
            self.screen.blit(text,(10,360))
            text = font.render("x: 9999 [mm]",True,(0,0,0))
            self.screen.blit(text,(10,390))
            text = font.render("y: 9999 [mm]",True,(0,0,0))
            self.screen.blit(text,(10,420))
            text = font.render("t: 9999 [deg]",True,(0,0,0))
            self.screen.blit(text,(10,450))
 
            text = font.render("    VELOCITY    ",True,(0,0,0))
            self.screen.blit(text,(150,360))
            text = font.render("dxy/dt: 9999 [mm/s]",True,(0,0,0))
            self.screen.blit(text,(150,390))
            text = font.render("dth/dt: 9999 [deg/s]",True,(0,0,0))
            self.screen.blit(text,(150,420))
            text = font.render("dt:     9999 [s]",True,(0,0,0))
            self.screen.blit(text,(150,450))

            # Plot Buttons
            self.screen.blit(self.arrowup,(438,325))
            self.screen.blit(self.arrowdown,(438,400))
            self.screen.blit(self.arrowleft,(363,400))
            self.screen.blit(self.arrowright,(513,400))
            self.screen.blit(self.resetbut,(513,325))

            pygame.display.flip()


MainWindow = MainClass()
MainWindow.MainLoop()
